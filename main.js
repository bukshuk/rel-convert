const path = require("path");
const { app, BrowserWindow, ipcMain, shell } = require("electron");
const ffmpeg = require("fluent-ffmpeg");
const { ffprobe } = ffmpeg;

let mainWindow;

app.on("ready", () => {
  mainWindow = new BrowserWindow({
    height: 600,
    width: 800,
    webPreferences: {
      backgroundThrottling: false,
    },
  });
  mainWindow.loadURL(`file://${__dirname}/src/index.html`);
});

ipcMain.on("add:videos", (_, videos) => {
  const tasks = videos.map(
    (video) =>
      new Promise((resolve, reject) =>
        ffprobe(video.path, (err, metadata) => {
          if (!err) {
            resolve(
              Object.assign(
                { duration: metadata.format.duration, format: "avi" },
                video
              )
            );
          } else {
            reject(err);
          }
        })
      )
  );

  Promise.all(tasks)
    .then((metadata) => {
      mainWindow.webContents.send("metadata:done", metadata);
    })
    .catch((err) => console.error(err));
});

ipcMain.on("conversion:start", (_, videos) => {
  for (const inputPath in videos) {
    const outputPath = getOutputPath(inputPath, videos[inputPath].format);
    ffmpeg(inputPath)
      .output(outputPath)
      .on("progress", ({ timemark }) => {
        mainWindow.webContents.send(
          "conversion:progress",
          Object.assign({ timemark }, videos[inputPath])
        );
      })
      .on("end", () =>
        mainWindow.webContents.send(
          "conversion:end",
          Object.assign({ outputPath }, videos[inputPath])
        )
      )
      .run();
  }
});

const getOutputPath = (inputPath, extention) => {
  const parts = path.parse(inputPath);
  return path.join(parts.dir, `${parts.name}.${extention}`);
};

ipcMain.on("folder:open", (_, filePath) => {
  shell.showItemInFolder(filePath);
});
