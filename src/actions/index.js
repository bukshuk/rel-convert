import { ipcRenderer } from "electron";
import {
  ADD_VIDEO,
  ADD_VIDEOS,
  REMOVE_VIDEO,
  REMOVE_ALL_VIDEOS,
  VIDEO_PROGRESS,
  VIDEO_COMPLETE,
} from "./types";

export const addVideos = (videos) => (dispatch) => {
  ipcRenderer.send("add:videos", videos);

  ipcRenderer.on("metadata:done", (_, metadata) =>
    dispatch({ type: ADD_VIDEOS, payload: metadata })
  );
};

export const convertVideos = () => (dispatch, getState) => {
  const { videos } = getState();

  ipcRenderer.send("conversion:start", videos);

  ipcRenderer.on("conversion:progress", (_, video) => {
    dispatch({
      type: VIDEO_PROGRESS,
      payload: { ...video },
    });
  });

  ipcRenderer.on("conversion:end", (_, video) => {
    dispatch({
      type: VIDEO_COMPLETE,
      payload: { ...video },
    });
  });
};

export const showInFolder = (outputPath) => (dispatch) => {
  ipcRenderer.send("folder:open", outputPath);
};

export const addVideo = (video) => {
  return {
    type: ADD_VIDEO,
    payload: { ...video },
  };
};

export const setFormat = (video, format) => {
  return {
    type: ADD_VIDEO,
    payload: { ...video, format, err: "" },
  };
};

export const removeVideo = (video) => {
  return {
    type: REMOVE_VIDEO,
    payload: video,
  };
};

export const removeAllVideos = () => {
  return {
    type: REMOVE_ALL_VIDEOS,
  };
};
